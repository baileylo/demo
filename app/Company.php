<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $fillable = ['id'];

    public function campaign()
    {
        return $this->hasMany(Campaign::class);
    }
}
